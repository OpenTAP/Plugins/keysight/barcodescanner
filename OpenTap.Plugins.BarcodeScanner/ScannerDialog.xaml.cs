﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Keysight.Ccl.Wsl.UI;

namespace OpenTap.Plugins.BarcodeScanner
{
    /// <summary>
    /// Interaction logic for ScannerDialog.xaml
    /// </summary>
    public partial class ScannerDialog : WslDialog
    {
        public string ScannerResults;

        public string UserTitle
        {
            get { return this.Title; }
            set { this.Title = value; }
        }

        public string UserHeading
        {
            get { return labelUserHeading.Content as string; }
            set { labelUserHeading.Content = value; }
        }

        public string UserDescription
        {
            get { return textUserDescription.Text; }
            set { textUserDescription.Text = value; }
        }

        private List<string> _userDescriptions = null;

        /// <summary>   If these are set, the dialog will prompt the user for every scan with the next entry from the list. This overrides the EntryLimit and the single UserDescription setting </summary>
        public List<string> UserDescriptions
        {
            get { return _userDescriptions; }
            set
            {
                _userDescriptions = value;
                if (_userDescriptions?.Count > 0)
                {
                    textUserDescription.Text = _userDescriptions[0];
                    LowerEntryLimit = UpperEntryLimit = _userDescriptions.Count;
                }
            }
        }

        public int? LowerEntryLimit { get; set; } = null;

        /// <summary>   Limits the amount of allowed input items. </summary>
        public int? UpperEntryLimit { get; set; } = null;

        public bool IsDuplicatesAllowed { get; set; } = false;

        public string LimitReachedInfo { get; set; } = "All entries have been recorded. Check your input and click OK or scan the 'DONE' code from the screen to confirm.";

        private BarcodeScannerInstrument _scanner;

        public BarcodeScannerInstrument ScannerDevice
        {
            get { return _scanner; }
            set
            {
                _scanner = value;
                if (_scanner != null)
                    _scanner.BarcodeReceivedEvent += _scanner_BarcodeReceivedEvent;
            }
        }
        
        public ScannerDialog(string imagePath)
        {
            InitializeComponent();
            var imageUri = new Uri(System.IO.Path.GetFullPath(imagePath));
            var ImageBitmap = new BitmapImage(imageUri);
            this.ImageBrush.ImageSource = ImageBitmap;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            this.Closed += ScannerDialog_Closed;
            this.textBoxID.KeyDown += new KeyEventHandler(textBoxID_KeyDown);
            this.Loaded += DiaglogLoaded;
            Topmost = true;
        }

        public void DiaglogLoaded(object sender, RoutedEventArgs e)
        {
            this.textBoxID.Focus();
        }

        private void ScannerDialog_Closed(object sender, EventArgs e)
        {
            if (_scanner != null)
                _scanner.BarcodeReceivedEvent -= _scanner_BarcodeReceivedEvent;
        }

        private void _scanner_BarcodeReceivedEvent(object sender, string e)
        {
            this.textBoxID.Text = e.Trim();            
            Exit();
        }
    
        private void textBoxID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ScannerResults = this.textBoxID.Text;
                Exit();
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            ScannerResults = this.textBoxID.Text;
            Exit();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void Exit()
        {
            DialogResult = true;
            this.Close();
        }

    }

}
