﻿// Author: MyName
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;


namespace OpenTap.Plugins.BarcodeScanner
{
    [Display("Scanner Input Dialog", Groups: new[] { "Barcode Scanner" }, Description: "Opens a dialog for scanning or entering multiple values.")]
    public class BarcodeScannerDialog : TestStep
    {
        #region Settings

        [Display("Use COM Port Scanner")]
        public bool UseComPortScanner { get; set; }

        [EnabledIf("UseComPortScanner", true, HideIfDisabled = true)]
        [Display("Scanner", Description: "The instrument to use for scanning.", Order: 1)]
        public BarcodeScannerInstrument ScannerDevice { get; set; }

        [Display("User Title", Description: "The title for the dialog window.", Order: 2)]
        public string UserTitle { get; set; }

        [Display("User Header", Description: "The headline for the user instructions.", Order: 3)]
        public string UserHeading { get; set; }

        [Display("User Description", Description: "The content for the user instructions.", Order: 4)]
        public string UserDescription { get; set; }

        [Display("Image", Order: 5)]
        [FilePath]
        public string ImageFilePath { get; set; }

    
        [Output]
        [Display("Barcode", Description: "The content for the user instructions.", Order: 5)]
        public string Barcode { get; private set; }

        #endregion Settings

        public BarcodeScannerDialog()
        {
            UserTitle = "Scanner Input";
            UserHeading = "DUT Scan";
            UserDescription = "Please scan the bar code on the device under test.";
            ImageFilePath = "ScannerImage.png";
        }

        public override void Run()
        {
            // Check if we are running in a GUI or a Console process
            if (System.Windows.Application.Current == null)
            {
                WpfDialogHost.StepReference = this;
                WpfDialogHost.ShowDialog();
                if (WpfDialogHost.DialogResult == true)
                    Success(WpfDialogHost.Dialog);
                else
                    Cancel();
            }
            else
            {
                System.Windows.Application.Current.Dispatcher.Invoke(() =>
                {
                    ScannerDialog dialog = CreateDialog();

                    if (dialog.ShowDialog() == true)
                        Success(dialog);
                    else
                        Cancel();
                });
            }
        }

        private static void Cancel()
        {
            throw new Exception("Bar code input was canceled. Ending Test");
        }

        private void Success(ScannerDialog dialog)
        {
            var ScanResults = new BarcodeResults { Barcode = dialog.ScannerResults };
            Results.Publish(ScanResults);
            Barcode = ScanResults.Barcode;
            Log.Info($"Barcode: {ScanResults.Barcode}"); 
            Verdict = Verdict.Pass;
        }

        public ScannerDialog CreateDialog()
        {
            return new ScannerDialog(ImageFilePath)
            {
                UserTitle = UserTitle,
                UserDescription = UserDescription,
                UserHeading = UserHeading,
            };
        }

        private class WpfDialogHost
        {
            // NOTE:
            // This is a temporary workaround to allow the teststep itself to display
            // a dialog while running within a server process.
            // Ultimately this has to be replaced completely by a dialog service that
            // will be presenting the interactive part and the teststep only issuing
            // the calls and handling the results.

            private static bool IsDone { get; set; }
            private static System.Windows.Application WpfApp = null;
            public static BarcodeScannerDialog StepReference { get; set; }
            public static bool? DialogResult { get; set; }

            public static ScannerDialog Dialog { get; private set; }

            [STAThread]
            public static void ShowDialog()
            {
                // Create the Application context on the first go
                if (WpfApp == null)
                {
                    Thread thread = new Thread(new ThreadStart(() =>
                    {
                        WpfApp = new System.Windows.Application();
                        WpfApp.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                        WpfApp.Run();
                    }));
                    thread.SetApartmentState(ApartmentState.STA);
                    thread.IsBackground = true; // Make sure we do not produce a zombie thread: A Background thread is automatically terminated on process shutdown (and other than that identical to a foreground thread).
                    thread.Start();

                    while (WpfApp == null)
                        System.Threading.Thread.Sleep(150);
                }

                // Use the Application context to create display the dialog
                IsDone = false;
                WpfApp.Dispatcher.Invoke(() =>
                {
                    Dialog = StepReference.CreateDialog();
                    DialogResult = Dialog.ShowDialog();
                    IsDone = true;
                });

                while (!IsDone)
                    System.Threading.Thread.Sleep(150);
            }
        }
    }

    internal class BarcodeResults
    {
        [DisplayName("Bar code string")]
        public string Barcode { get; set; }
    }
}

