﻿// Author: MyName
// Copyright:   Copyright 2019 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Text;
using System.IO.Ports;


namespace OpenTap.Plugins.BarcodeScanner
{
    [Display("Bar Code Scanner on COM", Groups: new[] { "Bar Code Scanners" }, Description: "Any bar code scanner that implements a real or virtual COM port")]
    public class BarcodeScannerInstrument : Instrument
    {
        public event EventHandler<string> BarcodeReceivedEvent;

        #region Settings

        [Display("COM Port", Groups: new[] { "Common" }, Description: "The COM Port that the scanner is connected to, e.g. 'COM1'")]
        public string ComPort { get; set; }

        public SerialPort _serialPort;

        #endregion Settings

        public BarcodeScannerInstrument()
        {
            ComPort = "COM1";
        }

        /// <summary>
        /// Open procedure for the instrument.
        /// </summary>
        public override void Open()
        {
            base.Open();
            if (!string.IsNullOrWhiteSpace(ComPort)) try
                {
                    _serialPort = new SerialPort(ComPort);
                    if (!_serialPort.IsOpen)
                    {
                        try
                        {
                            _serialPort.Open();
                            _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                        }
                        catch
                        {
                            throw new Exception("Can not connect to COM port");
                        }
                    }
                }
                catch
                {
                    throw new Exception("Can not connect to COM port");
                }
        }

        private void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // saves received data from serialport into buffer
            byte[] buffer = new byte[_serialPort.ReadBufferSize];

            // checks how many bytes were received
            int bytesRead = _serialPort.Read(buffer, 0, buffer.Length);

            // received data in ASCII code.
            string receivedString = Encoding.ASCII.GetString(buffer, 0, bytesRead);

            // removes \r from received data
            receivedString = receivedString.Trim();

            // calls Scanner.GetScannerText to add received data
            // Note that we also intentionally pass on empty data since a client
            // may use this for confirmation etc.
            BarcodeReceivedEvent?.Invoke(this, receivedString);
        }

        /// <summary>
        /// Close procedure for the instrument.
        /// </summary>
        public override void Close()
        {
            _serialPort?.Close();
            base.Close();
        }
    }

}
